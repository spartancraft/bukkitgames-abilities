package de.ftbastler.bgabilities;

import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import de.ftbastler.bukkitgames.api.AbilityExistsException;
import de.ftbastler.bukkitgames.api.BukkitGamesAPI;

public class BGAbilities extends JavaPlugin {

	private static Logger logger;
	private static BukkitGamesAPI api;
	
	public static BukkitGamesAPI getAPI() {
		return api;
	}
	
	public static Logger getPluginLogger() {
		return logger;
	}
	
	@Override
	public void onEnable() {
		logger = getLogger();
		api = BukkitGamesAPI.getApi();
		
		try {
			getAPI().registerNewAbility(50, "CookieLover", "When eating a cookie you will obtain strength.");
			getAPI().registerNewAbility(51, "FireEater", "You can shoot fireballs at enemys.");
			getAPI().registerNewAbility(52, "AppleThief", "When eating an apple you will obtain invisibility.");
			getAPI().registerNewAbility(53, "HolyPotato", "When eating a potato you will be able to see at night.");
			getAPI().registerNewAbility(54, "SnowballFighter", "Throwing snowballs at other player will apply blindness on them.");
			getAPI().registerNewAbility(55, "Butcher", "Pigs, cows and chickens will always drop a lot of meat.");
			getAPI().registerNewAbility(56, "Skydiver", "You can take a maximum of 2 hearts of fall damage. Damage you would have taken is transfered to players within a 5 block radius of you.");
			getAPI().registerNewAbility(57, "Arsonist", "When being on fire you will obtain strength and fire protection.");
			getAPI().registerNewAbility(58, "Forester", "When planting a tree of placing a seed they will grow instantly.");
			getAPI().registerNewAbility(59, "Rioter", "Hitting a player with your blaze rod has a chance to break his current item.");
			getAPI().registerNewAbility(60, "Thief", "Hitting a player with your stick has a chance to steal the current item of your victom.");
			getAPI().registerNewAbility(61, "Undead", "Mobs won't attack you until you attack them first.");
			Bukkit.getPluginManager().registerEvents(new AbilitiesListener(), this);
			getPluginLogger().info("Enabled extended abilities! (v" + getDescription().getVersion() + ")");
		} catch (AbilityExistsException e) {
			e.printStackTrace();
		}
	}

}
