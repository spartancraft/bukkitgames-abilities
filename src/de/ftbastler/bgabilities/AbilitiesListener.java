package de.ftbastler.bgabilities;

import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.CropState;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.TreeType;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import de.ftbastler.bukkitgames.api.BukkitGamesAPI;
import de.ftbastler.bukkitgames.enums.GameState;

public class AbilitiesListener implements Listener {

	Logger log = BGAbilities.getPluginLogger();
	BukkitGamesAPI api = BGAbilities.getAPI();
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		Action a = event.getAction();
		
		if(p == null)
			return;
		
		//ab# 50 CookieStrengh
		
		if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
			if ((api.getPlayerCanUseAbility(p, 50, false) & p.getItemInHand()
					.getType() == Material.COOKIE)) {
				api.setPlayerCooldown(p, 50, 5);
				p.addPotionEffect(new PotionEffect(
						PotionEffectType.INCREASE_DAMAGE, 8 * 20, 0));
				p.getInventory().removeItem(
						new ItemStack[] { new ItemStack(Material.COOKIE, 1) });
				p.playSound(p.getLocation(), Sound.BURP, 1, 1);
			}
		}
		
		//ab# 51 ShootFireballs
		
		if (a == Action.LEFT_CLICK_BLOCK || a == Action.LEFT_CLICK_AIR) {
			if (api.getPlayerCanUseAbility(p, 51, true) && p.getItemInHand() != null && 
					p.getItemInHand().getType().equals(Material.FIREBALL)) {
				Vector lookat = p.getLocation().getDirection().multiply(10);
				Fireball fire = p.getWorld().spawn(p.getLocation().add(lookat), Fireball.class);
				fire.setShooter(p);
				p.playSound(p.getLocation(), Sound.FIRE, 1.0F, (byte) 1);
				p.getInventory().removeItem(new ItemStack[] { new ItemStack(Material.FIREBALL, 1) });
			}
		}
		
		try{
			//ab# 52 AppleInvisibility
			
			if (api.getPlayerCanUseAbility(p, 52, true) && p.getItemInHand()
					.getType() == Material.APPLE && (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK)) {
				
					api.setPlayerCooldown(p, 52, 15);
										
					p.getInventory().removeItem(new ItemStack[] { new ItemStack(Material.APPLE, 1) });
					p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10 * 20, 1));
					p.playSound(p.getLocation(), Sound.PORTAL_TRIGGER, 1.0F, (byte) 1);
			}
			
			//ab# 53 PotatoNightvision
			
			if (api.getPlayerCanUseAbility(p, 53, true) && p.getItemInHand()
					.getType() == Material.POTATO && (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK)) {
				
				api.setPlayerCooldown(p, 53, 30);
				p.getInventory().removeItem(new ItemStack[] { new ItemStack(Material.POTATO, 1) });
				p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 20 * 20, 1));
				p.playSound(p.getLocation(), Sound.ENDERMAN_IDLE, 1.0F, (byte) 1);
			}
		
		} catch(NullPointerException e) {
			e.printStackTrace();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onProjectileHit(ProjectileHitEvent event) {
		Projectile entity =  event.getEntity();
		
		//ab# 54 SnowballBlindness
		
		if (entity.getType() == EntityType.SNOWBALL) {
			Snowball ball = (Snowball) entity;
			LivingEntity shooter = (LivingEntity) ball.getShooter();
			if (shooter.getType() == EntityType.PLAYER) {
				Player player = (Player) shooter;
				if(api.getPlayerIsSpectator(player)) {
					return;
				}
				if (api.getPlayerCanUseAbility(player, 54, true).booleanValue()) {
					Bukkit.getServer().getWorlds().get(0)
							.createExplosion(ball.getLocation(), 0.0F);
					for (Entity e : ball.getNearbyEntities(3.0D, 3.0D, 3.0D))
						if ((e.getType() == EntityType.PLAYER)) {
							Player pl = (Player) e;
							if (pl.getName() != player.getName() && !api.getPlayerIsSpectator(pl)) {
								pl.addPotionEffect(new PotionEffect(
										PotionEffectType.BLINDNESS, 100, 1));
								pl.addPotionEffect(new PotionEffect(
										PotionEffectType.CONFUSION, 160, 1));
							}
						}
				}
			} else {
				return;
			}
		}
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDeath(EntityDeathEvent e) {
		Player p = e.getEntity().getKiller();
		
		//ab# 55 AnimalKiller
		
		if (e.getEntity() != null && e.getEntity().getKiller() != null && 
				api.getPlayerCanUseAbility(p, 55, false)) {
				
				if(e.getEntityType() == EntityType.PIG) {
					e.getDrops().clear();
					e.getDrops().add(new ItemStack(Material.PORK, 3));
					p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
				} else if(e.getEntityType() == EntityType.COW) {
					e.getDrops().clear();
					e.getDrops().add(new ItemStack(Material.RAW_BEEF, 3));
					p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
				} else if(e.getEntityType() == EntityType.CHICKEN) {
					e.getDrops().clear();
					e.getDrops().add(new ItemStack(Material.RAW_CHICKEN, 3));
					p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
				}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamage(EntityDamageEvent event) {		
		if (event.getEntity() instanceof Player) {
			Player p = (Player) event.getEntity();
			if(api.getPlayerIsSpectator(p))
				return;
			
			//ab# 56 FalldamageTransfer
			
			if (api.getPlayerCanUseAbility(p, 56, true)) {
				if (event.getCause() == DamageCause.FALL) {
					if (event.getDamage() > 4) {
						event.setCancelled(true);
						p.damage(4);
					}
					p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
					List<Entity> nearbyEntities = event.getEntity().getNearbyEntities(5, 5, 5);
					for (Entity target : nearbyEntities) {
						if (target instanceof Player) {
							Player t = (Player) target;
							if(api.getPlayerIsSpectator(t))
								continue;
							if(t.getName() == p.getName())
								continue;
							if (t.isSneaking())
								t.damage(event.getDamage() / 2, event.getEntity());
							else
								t.damage(event.getDamage(), event.getEntity());
						}
					}
				}
			}
			
			//ab# 57 FireStrengh
			
			if (event.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK) {
				if ((api.getPlayerCanUseAbility(p, 57, false) & !p.hasPotionEffect(PotionEffectType.FIRE_RESISTANCE))) {
					p.addPotionEffect(new PotionEffect(
							PotionEffectType.INCREASE_DAMAGE, 200, 1));
					p.addPotionEffect(new PotionEffect(
							PotionEffectType.FIRE_RESISTANCE, 260, 1));
					p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
				}
			}
			
				if(api.getPlayerCanUseAbility(p, 18, false)) {
					event.setDamage(event.getDamage() - 1);
				}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockPlace(BlockPlaceEvent event) {
		Block block = event.getBlockPlaced();
		Player p = event.getPlayer();
		
		//ab# 58 TreeGrower
		
		if (api.getPlayerCanUseAbility(p, 58, false) && block.getType() == Material.CROPS) {
			p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
			block.setData(CropState.RIPE.getData());
		}
		if (api.getPlayerCanUseAbility(p, 58, false) && block.getType() == Material.MELON_SEEDS) {
			p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
			block.setData(CropState.RIPE.getData());
		}
		if (api.getPlayerCanUseAbility(p, 58, false) && block.getType() == Material.PUMPKIN_SEEDS) {
			p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
			block.setData(CropState.RIPE.getData());
		}
		if (api.getPlayerCanUseAbility(p, 58, false) && block.getType() == Material.SAPLING) {
			p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
			TreeType t = getTree(block.getData());
			Bukkit.getServer().getWorlds().get(0).generateTree(block.getLocation().subtract(0, 1, 0) , t);
		}
	}
	
    public TreeType getTree(int data) {
        TreeType tretyp = TreeType.TREE;
        switch(data) {
        case 0:
            tretyp = TreeType.TREE;
            break;
        case 1:
            tretyp = TreeType.REDWOOD;
            break;
        case 2:
            tretyp = TreeType.BIRCH;
            break;
        case 3:
            tretyp = TreeType.JUNGLE;
            break;
        default:
            tretyp = TreeType.TREE;
        }
        return tretyp;
    }
    
    
    
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		Entity damager = event.getDamager();
		Entity defender = event.getEntity();
		
		if (api.getCurrentGameState() == GameState.PREGAME && !(event.getEntity() instanceof Player)) {
			return;
		}
		
		if (api.getCurrentGameState() != GameState.RUNNING && event.getEntity() instanceof Player) {
			return;
		}
		
		if (event.getEntity().isDead()) {
			return;
		}

		
		if (damager instanceof Player) {
			
			Player dam = (Player)damager;
			
			if (defender.getType() == EntityType.PLAYER) {
				
				Player def = (Player)defender;
				
				//ab# 59 ItemBreaker
				
				if(api.getPlayerCanUseAbility(dam, 59, true) && dam.getItemInHand().getType() == Material.BLAZE_ROD && def.getItemInHand() != null) {
						int random = (int) (Math.random()* 5-1)+1;
						if (random == 1) {
							api.setPlayerCooldown(dam, 59, 15);
							def.getInventory().clear(def.getInventory().getHeldItemSlot());
							dam.sendMessage(ChatColor.DARK_PURPLE + "You broke your enemy's item.");
							def.sendMessage(ChatColor.DARK_PURPLE + "An enemy broke one of your items!");
							dam.playSound(dam.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
						}
				}
				
				//ab# 60 ItemThief
				if(api.getPlayerCanUseAbility(dam, 60, true) && dam.getItemInHand().getType() == Material.STICK && def.getItemInHand() != null) {
					

						int random = (int) (Math.random()* 6-1)+1;
						if(random == 1) {
							api.setPlayerCooldown(dam, 60, 15);
							dam.getInventory().clear(dam.getInventory().getHeldItemSlot());
							dam.getInventory().addItem(def.getItemInHand());
							def.getInventory().clear(def.getInventory().getHeldItemSlot());
							dam.sendMessage(ChatColor.DARK_PURPLE + "You stole on of your enemy's items.");
							def.sendMessage(ChatColor.DARK_PURPLE + "An enemy stole one of your items.");
							dam.playSound(dam.getLocation(), Sound.ORB_PICKUP, 1.0F, (byte) 1);
						}
				}
			}
		}
	}	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityTarget(EntityTargetEvent event) {
		Entity entity = event.getTarget();
		
		//ab# 61 MobFriendly
		
		if (entity != null) {
			if (entity instanceof Player) {
				Player player = (Player)entity;
				if(api.getPlayerCanUseAbility(player, 61, false) && event.getReason() == TargetReason.CLOSEST_PLAYER) {
					event.setCancelled(true);
				}
			}
		}
	}
}